/**
 * Created by J Won on 29/04/2017.
 */
$(document).ready(function() {

    //1. when radio button selected the background changes

    //selects the input radio with name radio and starts the function

    $("input:radio[name=radio]").click(function(){

        var selectedImage;

        console.log($('input[type="radio"]:checked').attr("id")); //this is a testing log to ensure to check that images are being changed

        // switching between options depending on the radio ID

        switch ($('input[type="radio"]:checked').attr("id")){

            case ("radio-background1"):
                selectedImage = "../images/ex4/background1.jpg";
                break;

            case ("radio-background2"):
                selectedImage = "../images/ex4/background2.jpg";
                break;

            case ("radio-background3"):
                selectedImage = "../images/ex4/background3.jpg";
                break;

            case ("radio-background4"):
                selectedImage = "../images/ex4/background4.jpg";
                break;

            case ("radio-background5"):
                selectedImage = "../images/ex4/background5.jpg";
                break;

            case ("radio-background6"):
                selectedImage = "../images/ex4/background6.gif";
                break;

            default:
                selectedImage = "../images/ex4/background1.jpg";
                break;
        }

// changing the src property of the background with the selected Image.
        $("#background").prop("src", selectedImage);
    });



    //2. enabling the check boxes to pick which porpoise will display

    // $("input:checkbox[type=checkbox]").change(function () {
    //     console.log($("input:checkbox:checked"));
    //     $("#check-dolphin1").click(function () {
    //         if(this.checked) {
    //             $("#dolphin1").show();
    //         }
    //         else {
    //             $("#dolphin1").hide();
    //         }
    //
    //     })
    // });

//default value hide for all dolphins except dolphin 1
    for (var dolphin = 2; dolphin <= 8; dolphin++){
        $("#dolphin" + dolphin).hide();
    }


    $("#check-dolphin1").click(function () {
        if(this.checked) {
            $("#dolphin1").show();
        }
        else {
            $("#dolphin1").hide();
        }
    })
    $("#check-dolphin2").click(function () {
        if(this.checked) {
            $("#dolphin2").show();
        }
        else {
            $("#dolphin2").hide();
        }
    })
    $("#check-dolphin3").click(function () {
        if(this.checked) {
            $("#dolphin3").show();
        }
        else {
            $("#dolphin3").hide();
        }
    })
    $("#check-dolphin4").click(function () {
        if(this.checked) {
            $("#dolphin4").show();
        }
        else {
            $("#dolphin4").hide();
        }
    })
    $("#check-dolphin5").click(function () {
        if(this.checked) {
            $("#dolphin5").show();
        }
        else {
            $("#dolphin5").hide();
        }
    })
    $("#check-dolphin6").click(function () {
        if(this.checked) {
            $("#dolphin6").show();
        }
        else {
            $("#dolphin6").hide();
        }
    })
    $("#check-dolphin7").click(function () {
        if(this.checked) {
            $("#dolphin7").show();
        }
        else {
            $("#dolphin7").hide();
        }
    })
    $("#check-dolphin8").click(function () {
        if(this.checked) {
            $("#dolphin8").show();
        }
        else {
            $("#dolphin8").hide();
        }
    })

        $(".checkbox").on("change", function(){
            if($(this).is("checked"))
            {
                if ($(this).attr("id") === "dolphin1"){
                }
            }
        })

    //3. creating a slider using Jquery UI to initialise the slider form controls. This slider is to scale any displayed images. ie. alter any shown 'dolphin' class images.

    // create a slider:
// var originalWidth = ("#dolphin1").width();

        $( ".slider" ).slider({
            value: 0,
            min: -50,
            max: 50,
            step: 10,
            slide: function (event, ui)
            {
                $(".dolphin").css("transform","scale(" + (1+(ui.value/100)) + ", " + (1+(ui.value/100)));
                $(".dolphin").css("-ms-transform","scale(" + (1+(ui.value/100)) + ", " + (1+(ui.value/100)));
                $(".dolphin").css("-webkit-transform:","scale(" + (1+(ui.value/100)) + ", " + (1+(ui.value/100)));

                // var multiplier = (1 + (ui.value / 100));

                // (".dolphin").width(newWidth);

                // (".dolphin").css("-ms-transform:", "\"" + multiplier + "\"");
            }
            //     var fraction = 1 + ui.value / 100), newWidth = (".dolphin").width() * (1 + ui.value / 100);
            // }
        });


});